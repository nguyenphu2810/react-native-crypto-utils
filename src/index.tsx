import { NativeModules } from 'react-native';

type CryptoUtilsType = {
  multiply(a: number, b: number): Promise<number>;
};

const { CryptoUtils } = NativeModules;

export default CryptoUtils as CryptoUtilsType;
