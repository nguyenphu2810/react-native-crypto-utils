# react-native-crypto-utils

crypto utils

## Installation

```sh
npm install react-native-crypto-utils
```

## Usage

```js
import CryptoUtils from "react-native-crypto-utils";

// ...

const result = await CryptoUtils.multiply(3, 7);
```

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
